
<?php

//Función para evaluar el primer y segundo término para así ser utilizado en Regula Falsi
function f($x,$coef,$ind){
	$valor = 0;
	$elevado = count($coef);
	for ($i=0; $i < count($coef); $i++) { 
		$valor = $valor+$coef[$i]*pow($x,($elevado-$i));
	}

	$valor = $valor+$ind;
	return $valor;
}

//Permite una precisión máxima de 0.00000000000001;
function regulaFalsi($a,$b,$e,$coef,$ind){
	$step = 1;
	$condition = true;
	$m = 0;
	$nan = 0;
	
	while ($condition) {
		$denominador = ( f($b,$coef,$ind) - f($a,$coef,$ind));
		if ($denominador == 0){
			$nan = 1;
			echo '<a><i class="material-icons left">error</i>División entre cero, no se puede efectuar el método.</a>';
			break;
		} else {
			$m = $a - ($b-$a) * f($a,$coef,$ind) / $denominador;
			$error = f($m,$coef,$ind);
			echo "<blockquote>Iteración: $step. Raíz: $a. Error: $error.</blockquote>";
			if ((f($a,$coef,$ind)*f($m,$coef,$ind)) < 0 ) {
				$b = $m;
			}
			else {
				$a = $m;
			}
	
			$step = $step+1;
			$condition = abs(f($m,$coef,$ind)) > $e;
		}
	

	}
	if (!$nan){
		echo "<strong><p>La raíz requerida es $m.</p></strong>";
	}
}

//Función para verfiicar si los intervalos ingresados contienen la raíz del polinomio
function verificacion($a,$b,$e,$coef,$ind){
	if ( (f($a,$coef,$ind)*f($b,$coef,$ind)) > 0 )  {
		echo '<a><i class="material-icons left">error</i>Los intervalos no encierran ninguna raíz. Intente ingresando otros.</a>';
	}
	else {
		regulaFalsi($a,$b,$e,$coef,$ind);
	}
}

?>