<?php 
include_once "../plantillas/header.php" ;
include_once "regulaf.php";
?>

<header>
	<div class="container">
		<h1>M&eacute;todo de Regula Falsi</h1>
	</div>
</header>

<form action="index.php" method="POST" class="form">
	<div class="container">
		<div class="row">
			<div class="input-field col s12">
				<input id="numero" type="number" name="grado" class="numero" min="1" max="4" required>
				<label for="numero">Grado del polinomio</label>
				<button class="btn waves-effect waves-light orange lighten-2" type="submit" onsubmit="cerrarVentana()" name="action"><i class="large material-icons">keyboard_return</i></button>
			</div>
		</div>
	</div>
</form>

<form action="index.php" method="POST" class="form">
	<?php 
		if(isset($_POST["grado"])){
			$grado = $_POST["grado"];
			for ($i=$grado; $i > 0; $i--) {
	?>

	<div class="container">
		<div class="row">
			<div class="input-field col s6">
				<input id="valor_x" type="number" name="coef[]" class="numero" required>
				<label for="valor_x">X<sup><?=$i?></sup></label>
			</div>
		</div>
	</div>

	<?php
			}
	?>

	<div class="container">
		<div class="row">
			<div class="input-field col s6">
				<input id="valor_x" type="number" name="ind" class="numero" required>
				<label for="valor_x">Valor independiente</label>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="input-field col s6">
				<input id="valor_a" type="number" name="a" class="numero" required>
				<label for="valor_a">Primer intervalo</label>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="input-field col s6">
				<input id="valor_b" type="number" name="b" class="numero" required>
				<label for="valor_b">Segundo intervalo</label>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="input-field col s6">
				<input id="error" type="number" step="any" name="error" class="numero" min="0.00000000000001" required>
				<label for="error">Índice de error</label>
				<button class="btn waves-effect waves-light orange lighten-2" type="submit"><i class="large material-icons">keyboard_return</i></button>
			</div>
		</div>
	</div>

	<?php 
		}
	?>
</form>

<div class="container">
	<div class="row">
		<?php 
			if(isset($_POST["coef"])){
				$a = $_POST["a"];
				$b = $_POST["b"];
				$e = $_POST["error"];
				$coef = $_POST["coef"];
				$ind = $_POST["ind"];
				verificacion($a,$b,$e,$coef,$ind);
			}
		?>
	</div>
</div>


</body>
</html>