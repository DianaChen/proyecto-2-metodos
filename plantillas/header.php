<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script type="text/javascript" src="../js/materialize.min.js"></script>
	<script type="text/javascript" src="../js/cerrar.js"></script>
	<title>Proyecto 2</title>
</head>
<body>
	<nav class="black">
		<div class="nav-wrapper">
			<a href="../index.html" class="brand-logo">Parcial</a>
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<li ><a href="../parte1/index.php">Regula Falsi</a></li>
				<li><a href="../parte2/index.php">Lin-Bairstow</a></li>
			</ul>
		</div>
	</nav>