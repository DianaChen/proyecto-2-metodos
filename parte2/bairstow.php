<?php

//Función para calcular coeficientes con la fórmula que se repite.
function calcularCoeficientes($coeficiente,$r,$s,$penultimo,$ultimo){
    return $coeficiente+$r*$penultimo+$s*$ultimo;
}

//Función para calcular delta R y S.
function calcularDeltas($coef,$ind,$r,$s){
    $deltas = [];

    if (count($coef)==3){
        $b3 = $coef[0];
        $b2 = $coef[1]+$r*$b3;
        $b1 = calcularCoeficientes($coef[2],$r,$s,$b2,$b3);
        $b0 = calcularCoeficientes($ind,$r,$s,$b1,$b2);
        $c3 = $b3;
        $c2 = $b2+$r*$c3;
        $c1 = calcularCoeficientes($b1,$r,$s,$c2,$c3);
        $denominador = pow($c2,2)-$c1*$c3;
        if ($denominador != 0){
            $deltaR = (-($c2*$b1-$c3*$b0)) / $denominador;
            $deltaS = (-($c2*$b0-$c1*$b1)) / $denominador;
            array_push($deltas,$deltaR,$deltaS);
        }
    }
    
    if (count($coef)==4){
        $b4 = $coef[0];
        $b3 = $coef[1]+$r*$b4;
        $b2 = calcularCoeficientes($coef[2],$r,$s,$b3,$b4);
        $b1 = calcularCoeficientes($coef[3],$r,$s,$b2,$b3);
        $b0 = calcularCoeficientes($ind,$r,$s,$b1,$b2);
        $c4 = $b4;
        $c3 = $b3+$r*$c4;
        $c2 = calcularCoeficientes($b2,$r,$s,$c3,$c4);
        $c1 = calcularCoeficientes($b1,$r,$s,$c2,$c3);
        $denominador = pow($c2,2)-$c1*$c3;
        if ($denominador != 0){
            $deltaR = ($c3*$b0-$c2*$b1) / $denominador;
            $deltaS = ($c1*$b1-$c2*$b0) / $denominador;
            array_push($deltas,$deltaR,$deltaS);
        }
    }

    elseif (count($coef)==5){
        $b5 = $coef[0];
        $b4 = $coef[1]+$r*$b5;
        $b3 = calcularCoeficientes($coef[2],$r,$s,$b4,$b5);
        $b2 = calcularCoeficientes($coef[3],$r,$s,$b3,$b4);
        $b1 = calcularCoeficientes($coef[4],$r,$s,$b2,$b3);
        $b0 = calcularCoeficientes($ind,$r,$s,$b1,$b2);
        $c5 = $b5;
        $c4 = $b4+$r*$c5;
        $c3 = calcularCoeficientes($b3,$r,$s,$c4,$c5);
        $c2 = calcularCoeficientes($b2,$r,$s,$c3,$c4);
        $c1 = calcularCoeficientes($b1,$r,$s,$c2,$c3);
        $denominador = pow($c2,2)-$c1*$c3;
        if ($denominador != 0){
            $deltaR = ($c3*$b0-$c2*$b1) / $denominador;
            $deltaS = ($c1*$b1-$c2*$b0) / $denominador;
            array_push($deltas,$deltaR,$deltaS);
        }
    }
    return $deltas;
}

//Función de Lin-Bairstow para sacar raíces cuadráticas.
function calcularRaices($r2,$s2){
    $raices = [];
    $raiz1 = ($r2+sqrt(pow($r2,2)+4*$s2))/2;
    $raiz2 = ($r2-sqrt(pow($r2,2)+4*$s2))/2;
    array_push($raices,$raiz1,$raiz2);
    return $raices;
}

//Función que llama a las funciones anteriores e imprime los resultados.
function bairstow($coef,$ind,$r,$s,$e){
    $condicion = 1;
    $arreglo = calcularDeltas($coef,$ind,$r,$s);
    if(empty($arreglo)){
        echo '<a><i class="material-icons left">error</i>División entre cero, no se puede efectuar el método.</a>';
    } else {
        $r1 = $r+$arreglo[0];
        $s1 = $s+$arreglo[1];
        $iteracion = 1;
        echo "<blockquote>Iteración: $iteracion. R: $r. S: $s.</blockquote>";
        do {
            $r=$r1;
            $s=$s1;
            $arreglo = calcularDeltas($coef,$ind,$r,$s);
            if(!empty($arreglo)){
                $r2=$r+$arreglo[0];
                $s2=$s+$arreglo[1];
                $errorR = abs($r1-$r2);
                $errorS = abs($s1-$s2);
                $iteracion= $iteracion + 1;
                echo "<blockquote>Iteración: $iteracion. R: $r1. S: $s1. Error de R: $errorR. Error de S: $errorS.</blockquote>";
                if( $errorR<$e && $errorS<$e ){
                    $raices = calcularRaices($r2,$s2);
                    if (is_nan($raices[0])){
                        echo '<p><a><i class="material-icons left">error</i>La primera raíz es imaginaria.</a></p>';
                    } else {
                        echo "<strong><p>La primera raíz es $raices[0].</p></strong>";
                    }
                    if (is_nan($raices[1])){
                        echo '<p><a><i class="material-icons left">error</i>La segunda raíz es imaginaria.</a></p>';
                    } else {
                        echo "<strong><p>La segunda raíz es $raices[1].</p></strong>";
                    }
                    $condicion = 0;
                } else {
                    $r1=$r2;
                    $s1=$s2;
                }
            } else{
                echo '<a><i class="material-icons left">error</i>División entre cero, no se puede efectuar el método.</a>';
                break;
            }
        } while ($condicion != 0);
    }
}
?>