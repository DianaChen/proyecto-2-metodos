<?php 
include_once "../plantillas/header.php" ;
include_once "bairstow.php";
?>

<header>
	<div class="container">
		<h1>M&eacute;todo de Lin-Bairstow</h1>
	</div>
</header>

<form action="index.php" method="POST" class="form">
	<div class="container">
		<div class="row">
			<div class="input-field col s12">
				<input id="numero" type="number" name="grado" class="numero" min="3" max="5" required>
				<label for="numero">Grado del polinomio</label>
				<button class="btn waves-effect waves-light orange lighten-2" type="submit" onsubmit="cerrarVentana()" name="action"><i class="large material-icons">keyboard_return</i></button>
			</div>
		</div>
	</div>
</form>

<form action="index.php" method="POST" class="form">
	<?php 
		if(isset($_POST["grado"])){
			$grado = $_POST["grado"];
			for ($i=$grado; $i > 0; $i--) {
	?>

	<div class="container">
		<div class="row">
			<div class="input-field col s6">
				<input id="valor_x" type="number" step="any" name="coef[]" class="numero" required>
				<label for="valor_x">X<sup><?=$i?></sup></label>
			</div>
		</div>
	</div>

	<?php
			}
	?>

	<div class="container">
		<div class="row">
			<div class="input-field col s6">
				<input id="valor_x" type="number" step="any" name="ind" class="numero" required>
				<label for="valor_x">Valor independiente</label>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="input-field col s6">
				<input id="valor_r" type="number" step="any" name="r" class="numero" required>
				<label for="valor_r">R</label>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="input-field col s6">
				<input id="valor_s" type="number" step="any" name="s" class="numero" required>
				<label for="valor_s">S</label>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="input-field col s6">
				<input id="error" type="number" step="any" name="error" class="numero" required>
				<label for="error">Índice de error</label>
				<button class="btn waves-effect waves-light orange lighten-2" type="submit"><i class="large material-icons">keyboard_return</i></button>
			</div>
		</div>
	</div>

	<?php 
		}
	?>
</form>

<div class="container">
	<div class="row">
		<?php 
			if(isset($_POST["coef"])){
				$r = $_POST["r"];
				$s = $_POST["s"];
				$e = $_POST["error"];
				$coef = $_POST["coef"];
				$ind = $_POST["ind"];
				bairstow($coef,$ind,$r,$s,$e);
			}
		?>
	</div>
</div>


</body>
</html>