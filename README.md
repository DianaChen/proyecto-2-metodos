# Métodos Proyecto 1

_Este proyecto se ha creado de una manera rudimentaria con el objetivo de ser parte de todo el proceso de la programación y lógica._

## Comenzando 🚀📋

```
- Para el desarrollo se utilizó PHP en su versión 5 y JavaScript
- Opera GX LVL2 (core: 73.0.3856.438).
- Chrome 90
- Xammp 3.2.4
- Wamp 3.2.3.
```

## Diseño ⚙️

```
- Podría mostrarse de manera diferente dependiendo de la versión del navegador.
- No ha sido optimizado para distintos tamaños de pantalla.
- Materialize 1.0.0
```

## Construido con 🛠️

* [CodeSansar](https://www.codesansar.com/numerical-methods/false-position-method-python-program.htm) - Explicación del método de Regula Falsi.
* [Lucas Silva](http://www.dailyfreecode.com/code/bairstows-method-2359.aspx) - Explicación del método de Lin-Bairstow.

## Autores ✒️

* **Diana Chen** - [@DianaChen]
* **Raúl Rojas** - [ultraSCAT](https://raulrojas1.github.io) 

## Expresiones de Gratitud 🎁
* Que el software sea libre como nosotros 📢 🍺 ☕ 🤓

---
